# Changelog

## 1.0.0 (2022-10-05)


### Features

* **package:** follown the plugin naming scheme ([e87dd8c](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/e87dd8ce15e9cefbb1dfd30c97cf65dc64dec8a7))


### Styles

* **python:** use `black` formatting ([df1d8c4](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/df1d8c47049ae5f067e47db8cf52306724d54ef6))


### Documentation

* **config:** the plugin is now named `radicale_storage_mongo` ([07512c3](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/07512c356b58e052f4ede9c4818ef38e993c7084))
* **contributing:** explain commit message format ([7223add](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/7223add6a94dce56d51f5bd0a990a614098b6e6e))
* **readme:** the plugin is now named `radicale_storage_mongo` ([9a55dcb](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/9a55dcb0a22cb091e04a2afc1813d887ab32a463))


### Continuous Integration

* **build:** build python distribution packages ([a6b8c1d](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/a6b8c1d1c290ae52edc467387536eba06159cbfc))
* **initial-checks:** enforce commit message format ([4532942](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/4532942a7ec9dc91a570bd9daffa3a4263218fa1))
* **release:** avoid regression in `dev` branch ([dc35fa3](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/dc35fa3d738f88221843d0d60e201d3c93daaf8c))
* **release:** create release automatically with `semantic-release` ([b2d35ee](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/b2d35ee58457501a17e64e02d5f31b200d603ff3))
* **release:** upload python packages to a PyPI registry ([0fa921a](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/0fa921a33e45933531149fb05a91ac5c86f4f511))
* **test:** enforce python black formatting of code ([32374b0](https://gitlab.mim-libre.fr/daniel.dehennin/radicale_mongo/commit/32374b0673c84ccbfc2d56f60c37bff978d40a19))
